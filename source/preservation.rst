.. include:: images.rst

.. _preservation-label:

Preservation
===============================================================================

.. Admonition:: Version

   This module was introduced in version 23.11 of Koha.

The preservation module can be accessed in two different ways: on the main page of the staff interface by
clicking 'Preservation' or through the 'More' menu on the top of the page.

The preservation module in Koha is used for integrating preservation treatments into the workflow and
keep track of them. For every single step of the preservation workflow, data is attached to the items. 
For each workflow, items go to a waiting list before being added to a train.

In order to use the module, you will first need to enable it using the  
:ref:`PreservationModule system preference<preservationmodule-syspref-label>`. Second,
:ref:`add the statuses<add-new-authorized-value-label>` you will need to track your items
throughout your workflow to the NOT_LOAN authorized value category.

- *Get there:* More > Preservation

.. _preservation-settings-label:

Settings
-------------------------------------------------------------------------------

The preservation settings are meant for two types of information:

#. storing the chosen item statuses (set up as NOT_LOAN :ref:`authorized values <authorized-values-label>`)
   for the workflow;

#. adding new processings that are meant to be attached to trains.
 
-  *Get there:* More > Preservation > Settings

|preservationsettings|

.. _preservation-general-settings:

General settings
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

In this section, you can set two different item statuses chosen from the NOT_LOAN
:ref:`authorized values <authorized-values-label>` category: 

-  Status for item added to waiting list: default items.notforloan status for each item entered in the
   waiting list (beginning of the preservation workflow).

-  Default status for item added to train: default items.notforloan status for any items entered in a
   train (monitoring of preservation workflow).

It is also possible to set these through the :ref:`PreservationNotForLoanWaitingListIn <preservationnotforloanwaitinglistin-label>`
and :ref:`PreservationNotForLoanDefaultTrainIn <preservationnotforloandefaulttrainin-label>` system preferences.

.. _preservation-processing-label:

Processings
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

To add a new processing, click on the 'Add new processing' button.

|newprocessing|

-  Processing name: this is the name that will appear in the drop-down menu when creating a new train. 

-  Letter template for printing slip: notice used to print a list of items in a train.

   -  If none are selected, you will not be able to generate a slip to print.

   -  You can use the default Train item slip (PRES_TRAIN_ITEM notice) or :ref:`create a new notice <adding-notices-and-slips-label>`.

   -  If you choose to :ref:`create a new notice <adding-notices-and-slips-label>`, make sure you
      select the module as Preservation. It may be easier to start by :ref:`copying the PRES_TRAIN_ITEM
      notice <copying-notices-and-slips>` rather than starting from scratch.

Each processing owns a list of attributes. To add a new attribute, click on the 'Add new attribute' button. 

-  Name: the name of the attribute. It will appear as one of the heads of the train table the processing has been attached to. 

-  Type: The type of data that will be needed for the train. It can be an authorized value, free text or a database column.

-  Options: appears if a type of authorized value or database column has been selected.
   It will show a drop-down menu of the possible values that are to be extracted for the said attribute. 

.. Tip ::

   For 'Type' and 'Options' fields, autocompletion is available: start typing in the box to quickly find
   the desired value.

.. Note ::

   Be sure to have your processings well figured out before creating them on Koha. While processings may be
   editable afterwards, we recommend not to do so in case they have already been attached to a train.
   
   If you do need to make changes, it is advised to create a new processing instead. 
    
.. _preservation-waitinglist-label:

Waiting list	
-------------------------------------------------------------------------------

The waiting list is the first step of the preservation workflow. It is to be considered as the Preservation module
entrance hall for the items.
 
-  *Get there:* More > Preservation > Waiting list

Adding an item the waiting list will update its 'Not for loan' status according to the value defined in the
:ref:`preservation settings <preservation-general-settings>`. As long as its status does not change again, the item
will remain in the waiting list.

This allows staff to make the item unavailable even though it has not yet been sent away for preservation. 

|waitinglist| 

.. _addto-waitinglist-label:

Add to waiting list
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

To add items to the waiting list, click on the 'Add to waiting list' button.

|addtowaitinglist|

A pop-up window appears, with an input field. Type in or scan your barcodes.

.. Tip::

   Enter one barcode per line, without spaces or commas. If barcodes are entered incorrectly, only the 
   last one in the list will be added.

After clicking 'Submit', your new items will show in the table.

A new button to 'Add last x items to a train' also appears. You can use this
if you want to immediately :ref:`add your items to a train <adding-items-from-waiting-list>` in a batch.

.. _preservation-trainspage-label:

Trains
-------------------------------------------------------------------------------

The trains page is the hub of the preservation module. This is where you will create and monitor
all your preservation trains. 

.. Note:: 

   The term 'train' has been chosen for two main reasons.
   
   -  Firstly, because other terminology - 'cart', 'basket', 'batch', etc. - was already in use
      for specific features in other Koha modules.
   
   -  Secondly, the term 'train' is commonly used by French librarians because the physical book carts
      look like linked wagons when lined up.
   
-  *Get there:* More > Preservation > Trains

|trainspage| 

This is the list of all your trains. The table contains the following information: 

- Name: the name you gave to the train

- Created on: the date the train was initiated 

- Closed on: the date you decided no item can be added to the train anymore

- Sent on: the date all the train items are physically sent to the bookbinder

- Received on: the date the train items are physically returned to the library

- Actions: 

   - Edit: to edit the train's settings 

   - Delete: to remove the train from the page

   - Add items: to directly add items from the waiting list to the train with a barcode

.. _new-train-label:
 
New train
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

To add a train, click on the 'New train' button.

|newtrain|

-  Name: the name you want to give to the train

-  Description: anything you would need to explain about this train

-  Status for item added to this train: choose from options set up in the NOT_LOAN
   :ref:`authorized values <authorized-values-label>` category. The default value proposed
   is the one you set in the :ref:`Preservation module settings <preservation-settings-label>`
   or :ref:`PreservationNotForLoanDefaultTrainIn <preservationnotforloandefaulttrainin-label>`
   system preference.

-  Default processing: the processing you want your train attached to. New processing are
   added in the :ref:`Preservation module settings <preservation-settings-label>`.

Click on the 'Submit' button to create the new train and return to the trains page.

.. _add-items-train-label:

Add items
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Once your train has been created, the next step is to add the items that will be sent away for
this round of preservation.

You can add items to a train:

-  one by one, :ref:`from the Trains page <adding-items-from-train>`;

-  in a batch, :ref:`from the Waiting list page <adding-items-from-waiting-list>`.

.. _adding-items-from-train:

Adding items from the Trains page
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

   .. Note::

      You have to :ref:`add your items to the waiting list <addto-waitinglist-label>`
      before you can add them to a train.

From the Trains page, either:

-  Click the 'Add items' button in the Actions column.

   -  Enter the item's barcode.

-  Or, click on a train's name.

   -  Click the 'Add items' button at the top of the page.

   -  Enter the item's barcode.

|addpreservationitem| 

The form displayed shows the item number and the attributes for the default processing (chosen when creating the train).

-  All the processing attributes that are database columns are filled with data from the relevant fields.
 
-  All the attributes are editable, even when extracted from the Koha database or the authorized values.

   .. Note:: 
      
      None of the modifications here will update the Marc data or the authorized values.
      The modified data is solely meant for use in the preservation module.
   
-  Each attribute can be duplicated.
    
|addpreservationitemmultivalued| 

You can also change the fields you need for this specific item by switching to another processing. 

   .. Note::
      
      If switching to another processing, the train's page will be unable to display a table summary. 

.. _adding-items-from-waiting-list:
 
Adding items from the waiting list
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

To gain time, you can add items to a train immediately after :ref:`adding them to the waiting
list <addto-waitinglist-label>`.

Click on the 'Add last x items to a train' button (where x refers to the number of items you just added).

|addlastitemstotrain| 

In the pop-up window that appears, select the desired train from the list of currently available
preservation trains; then click 'Submit'.

   .. Note::

      This will add your items to the train as a batch. You will not get the option to edit the
      processing attributes for individual items.

If you need to edit the processing attributes afterwards, you can do so from the train: use
the 'Edit' button in the Items table.

.. _train-management-label:

Train management
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

To view a preservation train, click on the train name on the Trains page table.

.. _show-train-label:

Show train
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

-  *Get there:* More > Preservation > Trains > Show train

The train allows you to perform four different actions:

-  :ref:`Add item <adding-items-from-train>`: click on the button and a pop-up to add an item
   will appear.

-  Edit: the edit button will send you back to the train settings you completed in order to
   :ref:`create the train <new-train-label>`.

-  Delete: deletes the entire train. The list of items in this train will disappear.

   .. Warning:: 
      
      When deleting an entire train, the items' Not for loan status does not automatically
      revert to the Available status.

-  Close / Sent / Received: a button to timestamp and monitor the workflow of your preservation train.

Under those four buttons you will see the displayed train's settings and the table of items included
in the train.

   .. Note::
      
      The column headers in the items table exactly match the attributes of the chosen
      :ref:`processing <preservation-processing-label>`.
      
      If the default NOT_LOAN values for the waiting list and the train are different, the item disappears
      from the waiting list when added to the train. It appears in both otherwise.

   .. Warning::
      
      It is recommended to keep the default processing the same throughout the whole workflow of the preservation
      train. Changing it while the train has items will disable the table view of items. 

|mypreservationtrain| 

The preservation train's items table contains the information selected when adding an item. 
On the right of the same table, you have three actions : 

-  Edit: to change the values of its preservation processing attributes.

-  Remove: to remove the item from the train.

   .. Warning:: 
      
      When removing an item, the item's Not for loan status does not
      automatically revert to the Available status. 

-  Print slip: when enabled in the :ref:`processing's default settings <preservation-processing-label>`,
   the 'Print slip' button appears in the actions.

   -  Check the box on the left of the items, then click on 'Print slips' at the top of the items table.

   -  If the default 'Train item slip' (notice code PRES_TRAIN_ITEM) is used, the generated slip will contain
      all of the items' information displayed in the table.

.. _close-send-train-label:

Close and send
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Once all the items have been added and edited, you will need to close the train.
Click on the 'Close' button on the top of the page. This adds a 'Closed on' date to the train details.

When a train has been closed, no new items can be added to it.

The next step is to physically send the train to the bookbinder. To keep track of this information in Koha,
click the 'Send' button where the 'Close' button used to be. 
It will update the status of the train to Sent and add a 'Sent on' date to the train details.

.. _receive-train-label:

Receive train
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Finally, when the train is physically returned to the library, click on the 'Receive' button.
This adds a 'Received on' date to the train details. 

|receivedtrain|

Occasionally, items arriving from the bookbinder have defects and need to be sent back.

You can move any items from your received train to another open train: use the 'Copy' button
in the Actions column and select the next train for this item to be sent on. Doing this
will keep the preservation data from the first train, though you can edit it.

   .. Note:: 
      
      If the preservation train contains items with different processings, the items table won't
      display. This is because each item would require a different table with different columns; they
      cannot coexist together in a unique view.
      
      In this case, the table will instead appear as a raw list of items with their processing data.
      
      |eclectictrain|