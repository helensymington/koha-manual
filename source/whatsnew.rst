.. include:: images.rst

.. _whats-new-label:

What's new
===============================================================================

This section highlights new features and enhancements in the latest Koha
releases.

This section is updated as the manual is updated. Therefore, it may not contain
all the new features and enhancements in the release. Please consult the release
notes to find an exhaustive list of all changes in Koha for each version.

.. _whats-new-23-11-label:

23.11
-------------------------------------------------------------------------------

.. Tip::

   `Read the full release notes for Koha 23.11.00 <https://koha-community.org/koha-23-11-released/>`_.

.. _whats-new-23-11-preservation:

Preservation module
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The brand new :ref:`Preservation module <preservation-label>` is used for integrating
preservation treatments into the Koha workflow and keep track of them. For every single
step of the preservation workflow, data is attached to the Koha items.

The module comes with its own set of :ref:`system preferences<preservation-system-preferences-label>`.

.. _whats-new-23-11-protected-patrons-label:

Protected patrons
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

It is now possible to protect patrons from deletion. When
:ref:`adding <add-a-new-patron-label>` or
:ref:`editing a patron <editing-patrons-label>`, a new 'Protected' flag can be
set in the 'Library management' section. This will disable the 'Delete' option
in the patron file. Furthermore, protected patrons cannot be deleted by batch
deletion, cron jobs, or patron merging.

Use this for your statistical patrons, SIP2 users, self checkout users and
superadmins.

.. _whats-new-23-11-custom-slips-label:

Custom slips
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

It is now possible to create custom slips that will be available from the
:ref:`'Print' menu in a patron's account <printing-receipts-label>`.

Go to :ref:`Tools > Notices and slips <notices-and-slips-label>` and
:ref:`create a new slip <adding-notices-and-slips-label>` with the new 'Patrons
(custom slip)' category.

.. _whats-new-23-11-custom-report-templates-label:

Custom report templates
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

It is now possible to create templates in the
:ref:`notices and slips tool <notices-and-slips-label>` and use those in the
:ref:`reports module <reports-label>` when
:ref:`running SQL reports <running-custom-reports-label>`.

.. _whats-new-23-11-vendor-issues-label:

Vendor issues
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

It is now possible to :ref:`record problems with vendors <vendor-issues-label>`
in the acquisitions module. It is a way to keep track of the various issues that
might arise in the course of a contract, and it might be helpful when the time
comes to renegotiate.

.. _whats-new-23-11-brach-level-userjs-usercss-label:

Library-level OPAC CSS and JS
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

It is now possible to add custom CSS and JavaScript to an OPAC for a specific
library. Two new fields were added to the
:ref:`library form <adding-a-library-label>`:

-  UserJS

-  UserCSS

This code will be used when a patron logs in to the OPAC, or if the system has
more than one OPAC (through apache configuration files).

.. _whats-new-23-11-offline-circulation-label:

Deprecation of the offline circulation module
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The :ref:`offline circulation module <offline-circulation-in-koha-label>` has
been deprecated. Only the :ref:`Firefox plugin <firefox-plugin-label>` and
:ref:`Windows tool <offline-circ-tool-for-windows-label>` are now supported.

Accordingly, the :ref:`AllowOfflineCirculation <allowofflinecirculation-label>`
system preference was removed.

.. _whats-new-23-11-html-customization-new-locations-label:

New display locations for HTML customizations
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

There are several new display locations for the
:ref:`HTML customization <html-customizations-label>` tool.

-  StaffAcquisitionsHome: content will appear at the bottom of the
   :ref:`Acquisitions module <acquisitions-label>` main page.

-  StaffAuthoritiesHome: content will appear at the bottom of the
   :ref:`Authorities <authorities-label>` main page.

-  StaffCataloguingHome: content will appear at the bottom of the
   :ref:`Cataloguing module <cataloging-label>` main page.

-  StaffListsHome: content will appear at the bottom of the
   :ref:`Lists <lists-label>` main page.

-  StaffPatronsHome: content will appear at the bottom of the
   :ref:`Patrons module <patrons-label>` main page.

-  StaffPOSHome: content will appear at the bottom of the
   :ref:`Point of sale <point-of-sale-label>` main page.

-  StaffSerialsHome: content will appear at the bottom of the
   :ref:`Serials module <serials-label>` main page.

The move of system preferences to HTML customizations continues. These were
moved to the :ref:`HTML customization tool <html-customizations-label>` in
version 23.11.

-  OpacMaintenanceNotice

-  OPACResultsSidebar

-  OpacSuppressionMessage

-  PatronSelfRegistrationAdditionalInstructions

-  SCOMainUserBlock

-  SelfCheckHelpMessage

-  SelfCheckInMainUserBlock

.. _whats-new-23-11-sysprefs-label:

New system preferences
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**AcquisitionsDefaultEMailAddress**

The new :ref:`AcquisitionsDefaultEMailAddress <acquisitionsdefaultemailaddress-label>`
allows you to set a specific email address that will be used to send orders and
late order claims from the acquisitions module.

**AcquisitionsDefaultReplyTo**

The new :ref:`AcquisitionsDefaultReplyTo <acquisitionsdefaultreplyto-label>`
allows you to set a specific reply-to email address that will receive replies to
orders and late order claims sent from the acquisitions module.

**CancelOrdersInClosedBaskets**

The new :ref:`CancelOrdersInClosedBaskets <cancelordersinclosedbaskets-label>`
system preference can be set to allow
:ref:`cancelling acquisitions orders <cancelling-an-order-label>` in baskets
that are closed. This is useful if something cannot be delivered and you don't
want to reopen the basket or go through the receive shipment process.

**DefaultAuthorityTab**

The new :ref:`DefaultAuthorityTab <defaultauthoritytab-label>` system preference
allows libraries to choose which tab is selected first when viewing an authority
record.

**ForceLibrarySelection**

The new :ref:`ForceLibrarySelection <forcelibraryselection-label>` can be used
to require staff to choose a library when logging into the staff interface.

**OverdueNoticeFrom**

The :ref:`OverdueNoticeFrom <overduenoticefrom-label>` system preference
already existed, but a new option was added to it in version 23.11. The new
option 'patron home library', allows libraries to choose that library as the
source of information for overdue notices.

**SerialsDefaultEMailAddress**

The new :ref:`SerialsDefaultEMailAddress <serialsdefaultemailaddress-label>`
allows you to set a specific email address that will be used to send late serial
issues claims from the serials module.

**SerialsDefaultReplyTo**

The new :ref:`SerialsDefaultReplyTo <serialsdefaultreplyto-label>`
allows you to set a specific reply-to email address that will receive replies to
late serial issues claims sent from the serials module.

**SerialsSearchResultsLimit**

The new :ref:`SerialsSearchResultsLimit <serialssearchresultslimit-label>` system
preference allows you to limit the number of serial subscription search results
per page, to be used in systems where there are a lot of subscriptions.

**showLastPatronCount**

The new :ref:`showLastPatronCount <showlastpatroncount-label>` system preference
allows you to choose how many patrons are shown by the link created by
:ref:`showLastPatron <showLastPatron-label>`.

**TrackLastPatronActivityTriggers**

The new :ref:`TrackLastPatronActivityTriggers <tracklastpatronactivitytriggers-label>`
system preference replaces the
:ref:`TrackLastPatronActivity <tracklastpatronactivity-label>` system preference
and allows a more granular control of which action triggers the update of the
patron's "last seen" date (borrowers.lastseen). Previously, this database column
was only updated when the patron logged into the OPAC or via SIP2. But this
excluded patrons who might check out a lot of items from the library, but never
log into the OPAC. The library can now decide which activities to track from a
list.

**UpdateItemLocationOnCheckout**

The new :ref:`UpdateItemLocationOnCheckout <updateitemlocationoncheckout-label>`
system preference allows you to automatically change an item's location when it
is checked out.

.. _whats-new-23-11-command-line-tools-label:

New options for command line tools
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The :ref:`runreport.pl <cron-runreport-label>` script has two new parameters:

-  :code:`--send_empty` adds the option to send the email even if the report
   returns no results

-  :code:`--quote` adds the option to specify the quote character for CSV output

The :ref:`writeoff_debts.pl <cron-writeoff-debt-label>` script has two new parameters:

-  :code:`--category-code` adds the option to limit writeoffs to a specific
   patron category

-  :code:`--added-after` adds the option to limit writeoffs to charges added
   after a specific or calculated date

The :ref:`borrowers-force-messaging-defaults.pl <cron-borrowers-messaging-preferences-label>`
script has two new parameters:

-  :code:`--library` adds the option to limit updates to patrons from a
   specific library

-  :code:`--message-name` adds the option to limit updates to a specific message

The :ref:`membership\_expiry.pl <cron-notify-patrons-of-expiration-label>`
script has four new parameters:

-  :code:`-active` adds the option to send notices to "active" patrons only.
   Activity is determined by the new
   :ref:`TrackLastPatronActivityTriggers <tracklastpatronactivitytriggers-label>`
   system preference.

-  :code:`-inactive` adds the option to send notices to "inactive" patrons only.
   Activity is determined by the new
   :ref:`TrackLastPatronActivityTriggers <tracklastpatronactivitytriggers-label>`
   system preference.

-  :code:`-renew` adds the option to automatically renew patron memberships
   instead of simply advising them that their membership is about to expire.

-  :code:`-letter_renew` adds the option to use a different notice than the
   default one, MEMBERSHIP\_RENEWED.